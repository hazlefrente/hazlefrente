<?php
/* Smarty version 3.1.33, created on 2020-06-17 20:31:29
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5eeac3f1435806_78020161',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1587040546,
      2 => 'module',
    ),
    '6f21551b854f987901f9a675003ad8e97320a44a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hazlefrente\\themes\\classic\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1587040546,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5eeac3f1435806_78020161 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Productos Destacados
  </h2>
  <div class="products">
          
  <article class="product-miniature js-product-miniature" data-id-product="6" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/hazlefrente/es/protector-facial/6-mug-the-best-is-yet-to-come.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/hazlefrente/31-home_default/mug-the-best-is-yet-to-come.jpg"
              alt="Protector Facial"
              data-full-size-image-url="http://localhost/hazlefrente/31-large_default/mug-the-best-is-yet-to-come.jpg"
            />
          </a>
              

      <div class="product-description">
        
                      <h2 class="h3 product-title" itemprop="name"><a href="http://localhost/hazlefrente/es/protector-facial/6-mug-the-best-is-yet-to-come.html">Protector Facial</a></h2>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">14,28 PEN</span>

              

              
            </div>
                  

        
          
        
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>
    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="16" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/hazlefrente/es/mascarillas/16-mountain-fox-notebook.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/hazlefrente/25-home_default/mountain-fox-notebook.jpg"
              alt="N95 1860"
              data-full-size-image-url="http://localhost/hazlefrente/25-large_default/mountain-fox-notebook.jpg"
            />
          </a>
              

      <div class="product-description">
        
                      <h2 class="h3 product-title" itemprop="name"><a href="http://localhost/hazlefrente/es/mascarillas/16-mountain-fox-notebook.html">N95 1860</a></h2>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">50,00 PEN</span>

              

              
            </div>
                  

        
          
        
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>
    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="21" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/hazlefrente/es/mascarillas/21-mountain-fox-notebook.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/hazlefrente/28-home_default/mountain-fox-notebook.jpg"
              alt="N95 8210"
              data-full-size-image-url="http://localhost/hazlefrente/28-large_default/mountain-fox-notebook.jpg"
            />
          </a>
              

      <div class="product-description">
        
                      <h2 class="h3 product-title" itemprop="name"><a href="http://localhost/hazlefrente/es/mascarillas/21-mountain-fox-notebook.html">N95 8210</a></h2>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">50,00 PEN</span>

              

              
            </div>
                  

        
          
        
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>
    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="22" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/hazlefrente/es/mascarillas/22-respirador-reutilizable-3m-6200.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/hazlefrente/30-home_default/respirador-reutilizable-3m-6200.jpg"
              alt="Respirador Reutilizable 3M..."
              data-full-size-image-url="http://localhost/hazlefrente/30-large_default/respirador-reutilizable-3m-6200.jpg"
            />
          </a>
              

      <div class="product-description">
        
                      <h2 class="h3 product-title" itemprop="name"><a href="http://localhost/hazlefrente/es/mascarillas/22-respirador-reutilizable-3m-6200.html">Respirador Reutilizable 3M...</a></h2>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">0,00 PEN</span>

              

              
            </div>
                  

        
          
        
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>
    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="23" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/hazlefrente/es/protector-facial/23-protector-facial-face-shield.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/hazlefrente/32-home_default/protector-facial-face-shield.jpg"
              alt="Protector Facial &quot;Face Shield&quot;"
              data-full-size-image-url="http://localhost/hazlefrente/32-large_default/protector-facial-face-shield.jpg"
            />
          </a>
              

      <div class="product-description">
        
                      <h2 class="h3 product-title" itemprop="name"><a href="http://localhost/hazlefrente/es/protector-facial/23-protector-facial-face-shield.html">Protector Facial &quot;Face Shield&quot;</a></h2>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">0,00 PEN</span>

              

              
            </div>
                  

        
          
        
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>
    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="24" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/hazlefrente/es/protector-facial/24-protector-facial-premium.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/hazlefrente/33-home_default/protector-facial-premium.jpg"
              alt="Protector Facial Premium"
              data-full-size-image-url="http://localhost/hazlefrente/33-large_default/protector-facial-premium.jpg"
            />
          </a>
              

      <div class="product-description">
        
                      <h2 class="h3 product-title" itemprop="name"><a href="http://localhost/hazlefrente/es/protector-facial/24-protector-facial-premium.html">Protector Facial Premium</a></h2>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">0,00 PEN</span>

              

              
            </div>
                  

        
          
        
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>
    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="25" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/hazlefrente/es/guantes/25-guantes-de-nitrilo.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/hazlefrente/34-home_default/guantes-de-nitrilo.jpg"
              alt="Guantes de Nitrilo"
              data-full-size-image-url="http://localhost/hazlefrente/34-large_default/guantes-de-nitrilo.jpg"
            />
          </a>
              

      <div class="product-description">
        
                      <h2 class="h3 product-title" itemprop="name"><a href="http://localhost/hazlefrente/es/guantes/25-guantes-de-nitrilo.html">Guantes de Nitrilo</a></h2>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">10,00 PEN</span>

              

              
            </div>
                  

        
          
        
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>
    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="26" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/hazlefrente/es/guantes/26-guantes-quirurgicos.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/hazlefrente/35-home_default/guantes-quirurgicos.jpg"
              alt="Guantes Quirúrgicos"
              data-full-size-image-url="http://localhost/hazlefrente/35-large_default/guantes-quirurgicos.jpg"
            />
          </a>
              

      <div class="product-description">
        
                      <h2 class="h3 product-title" itemprop="name"><a href="http://localhost/hazlefrente/es/guantes/26-guantes-quirurgicos.html">Guantes Quirúrgicos</a></h2>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">6,00 PEN</span>

              

              
            </div>
                  

        
          
        
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      
        <ul class="product-flags">
                      <li class="product-flag new">Nuevo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Vista rápida
          </a>
        

        
                  
      </div>
    </div>
  </article>

      </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://localhost/hazlefrente/es/2-inicio">
    Todos los productos<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
