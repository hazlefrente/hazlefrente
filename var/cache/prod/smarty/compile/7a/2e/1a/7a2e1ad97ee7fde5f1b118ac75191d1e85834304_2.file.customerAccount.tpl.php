<?php
/* Smarty version 3.1.33, created on 2020-06-17 20:27:06
  from 'C:\xampp\htdocs\hazlefrente\modules\psgdpr\views\templates\front\customerAccount.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5eeac2eac45bd1_30510274',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a2e1ad97ee7fde5f1b118ac75191d1e85834304' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hazlefrente\\modules\\psgdpr\\views\\templates\\front\\customerAccount.tpl',
      1 => 1592380931,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5eeac2eac45bd1_30510274 (Smarty_Internal_Template $_smarty_tpl) {
?>
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="identity-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('psgdpr','gdpr'), ENT_QUOTES, 'UTF-8');?>
">
    <span class="link-item">
        <i class="material-icons">account_box</i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'GDPR - Personal data','mod'=>'psgdpr'),$_smarty_tpl ) );?>

    </span>
</a>
<?php }
}
